/*
 * ViewController.swift
 * ConstraintBugDemo
 *
 * Created by François Lamboley on 2022/11/08.
 */

import UIKit



class ViewController: UIViewController {
	
	@IBOutlet var constraintBefore1: NSLayoutConstraint!
	@IBOutlet var constraintBefore2: NSLayoutConstraint!
	
	@IBOutlet var constraintAfter1: NSLayoutConstraint!
	@IBOutlet var constraintAfter2: NSLayoutConstraint!
	
	override func viewDidLoad() {
		super.viewDidLoad()
	}
	
	@IBAction func goToAfter() {
		NSLayoutConstraint.deactivate([constraintBefore1, constraintBefore2])
		NSLayoutConstraint.activate([constraintAfter1, constraintAfter2])
	}
	
}
